import { PipeTransform, Pipe } from "@angular/core";
@Pipe({
    name: 'rolesFilterPipe',
    pure: true

})
export class RolesFilterClass implements PipeTransform {

    transform(roles: any, name: string): any[] {
        if (!roles || !name) {
            return roles;
        }
        return roles.filter(e => e.name.toLowerCase().indexOf(name.toLowerCase()) !== -1);
    }
}