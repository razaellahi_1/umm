import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { service } from '../../services';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-create-new-role',
  templateUrl: './create-new-role.component.html',
  styleUrls: ['./create-new-role.component.scss']
})
export class CreateNewRoleComponent implements OnInit {
  Microservices;
  name;
  description;
  constructor(public dialogRef: MatDialogRef<CreateNewRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _callService: service) { }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: any = [];
  newArray: any = [];
  allFruits;
  @ViewChild('fruitInput') fruitInput: ElementRef;

  ngOnInit() {
    this.getMicroservices();
  }

  getMicroservices() {
    this._callService.getMicroservices().subscribe(
      (e) => {
      this.Microservices = e.microservices;

        const allPermissions = []
        e.microservices.map(function (x) {
          allPermissions.push(...x.permissions);
        })
        this.allFruits = allPermissions;

        this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
          startWith(null),
          map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));


      },
      (error) => { alert(error.message) }
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  add(event: MatChipInputEvent): void {
    debugger
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({
        id: Math.random(),
        name: value.trim()
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);

  }

  remove(fruit, indx): void {
    this.fruits.splice(indx, 1);
    this.newArray = this.fruits.map(res => { return { id: res.id } });
    console.log(JSON.stringify(this.newArray));


  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
    this.newArray = this.fruits.map(res => { return { id: res.id } });
    console.log(JSON.stringify(this.newArray));

  }

  private _filter(value: any): any[] {
    return this.allFruits.filter(fruit => fruit.name.toLowerCase().includes(value));
  }
  makeRole() {
    let role = { 'authority': this.name, 'description': this.description, 'createdBy': { 'id': 1 } };
    this._callService.CreateNewRole(role).subscribe(
      (e) => {
        console.log("role created");

        if (this.newArray.length > 0) {
          let roleId = e.role.id;
          let obj = { 'id': roleId, 'permissions': this.newArray, 'addRevoke': 'add' };
          this._callService.addRevokePermission(obj).subscribe(
            (e) => { console.log("permissions added") },
            (error) => { alert(error.message) }
          )
        }
      },
      (error) => { alert(error.message) }
    )
  }
}
