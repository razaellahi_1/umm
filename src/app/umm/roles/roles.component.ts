import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { service } from '../services';
import { MatDialog } from '@angular/material';
import { CreateNewRoleComponent } from './create-new-role/create-new-role.component';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  panelOpenState = false;
  customCollapsedHeight: string = '70px';
  customExpandedHeight: string = '70px';
  roles;
  spinner: boolean = false;
  connectionErrorImg = "assets/images/error.jpg";
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: any = [];
  newArray: any = [];
  allFruits;
  searchItem;
  @ViewChild('fruitInput') fruitInput: ElementRef;
  trackByRoletId = (index, role) => role.id;


  constructor(private dialog: MatDialog, private _callService: service) { }

  ngOnInit() {
    this.getUmmRoles();
    this.getMicroservices();
  }

  getUmmRoles() {
    this._callService.cast.subscribe(e => this.spinner = e);
    this._callService.getUmmRoles().subscribe(
      (e) => {
      this.roles = e.roles;
        this.spinner = false
      },
      (error) => {
        alert(error.message);
        this.spinner = false
      }
    )
  }

  onAddNewRoleClick() {
    const dialogRef = this.dialog.open(CreateNewRoleComponent, {
      maxWidth: '430px',
      maxHeight: '550px',
      width: '430px',
      height: '1200px'


    });

    dialogRef.afterClosed().subscribe(result => {

      if (result == 'ok') {
        this.getUmmRoles()
      }

    });
  }

  OnUpdateClick(role) {

    let newRole = { 'id': role.id, 'authority': role.name, 'description': role.description, 'createdBy': { 'id': 1 } };
    this._callService.updateRole(newRole).subscribe(
      (e) => { console.log("role updated") },
      (error) => { alert(error.message) }
    )
  }

  onClickDelete(id) {

    this._callService.deleteRole(id).subscribe(
      (e) => { this.getUmmRoles(); },
      (error) => { alert(error.message) }
    )
  }

  add(event: MatChipInputEvent): void {
    debugger
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({
        id: Math.random(),
        name: value.trim()
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);

  }

  remove(fruit, indx): void {
    this.fruits.splice(indx, 1);
    this.newArray = this.fruits.map(res => { return { id: res.id } });
    console.log(this.newArray);


  }

  selected(event: MatAutocompleteSelectedEvent, id): void {
    this.fruits.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
    this.newArray = this.fruits.map(res => { return { id: res.id } });
    let obj = { 'id': id, 'permissions': this.newArray, 'addRevoke':'add' };
    // console.log(JSON.stringify(obj));

    this._callService.addRevokePermission(obj).subscribe(
      (e) => {
        console.log("permission addede");
        let agentRolesObj = <any>[];
        agentRolesObj.push(e.role);
        this.roles = this.roles.map(h => agentRolesObj.find(o => o.id == h.id) || h);
      },
      (error) => { alert(error.message) }
    );
  }


  private _filter(value: any): any[] {
    return this.allFruits.filter(fruit => fruit.name.toLowerCase().includes(value));
  }

  getMicroservices() {
    this._callService.getMicroservices().subscribe(
      (e) => {
        
        const allPermissions = []
        e.microservices.map(function (x) {
          allPermissions.push(...x.permissions);
        })
        this.allFruits = allPermissions;

        this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
          startWith(null),
          map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
      },
      (error) => { alert(error.message) }
    )
  }

  onPermissionsClick(role, permissionId){
    let obj = { 'id': role.id, 'permissions': [{"id":permissionId}], 'addRevoke':'revoke' };
    // console.log(JSON.stringify(obj));
    this._callService.addRevokePermission(obj).subscribe(
      (e) => {
        console.log("permission remove");
        let agentRolesObj = <any>[];
        agentRolesObj.push(e.role);
        this.roles = this.roles.map(h => agentRolesObj.find(o => o.id == h.id) || h);
      },
      (error) => { alert(error.message) }
    );
  }

}
