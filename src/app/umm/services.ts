import { Observable,BehaviorSubject } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()

export class service {

    
    
    public spinner : boolean = false;
    private sender = new BehaviorSubject<boolean>(null);
    cast = this.sender.asObservable();
    token;
    baseUrl="http://192.168.1.234:9091";
    baseUrl1="https://192.168.1.76:8443";
    constructor(private _httpClient:HttpClient) { 
        this.token = "eyJhbGciOiJIUzI1NiJ9.eyJwcmluY2lwYWwiOiJINHNJQUFBQUFBQUFBSlZTUDBcL2JRQlJcL0RvbEFRbENvVkNRR3VoUzJ5cEhhTVZOQmdJU3NwR3FhQlNUUXhYNjRCK2M3OSs0TXlZSXl3Y0FBb2tXcTJxXC9BTjRHRkQxQ1ZnWldabFhlRzRNQ0Nlb3V0ZHpcL1wvXC9qMmYzVURGYVBnWWE4YUY4Vk9SeFZ6Nkp0VmN4Z2JEVEhQYjlUT0RPa0tiSTVaellJc21jSCs4RW5nQmxIaGs0WFd3eFhaWVZUQVpWeHZ0TFF4dHJhUGhnOUx4QStPbVpnbnVLcjN0UDNLSFN1TVRnWUxhKzEyQzRWV1laR0dvTW1uclNpNTJVcTR4V29XSlloYW9jTnVOM29SMGc5SnlKc3dnZEJnbGF3dU1BaGhsbWYybVNKV2pzZkRxM214bXVhZzIwZFlDR0VtWk1lVHVXWkttZGRiZHZiTXBLY0YzMklOeUpcL1hvVUhkekR1bzdIbjlCQ1VHcHVaSm10aVVURmZGTjdzU0p2emR6Y25uMHA5Y3FBVkFuNzFcLytwcGhQejBQdmZQMzJiVjYwRjFxWUdyQmV3R3FkbE54TUZzeGZOVHJsdjc4K1wvemk5T1ZnYkltV0hXUHJcL2ZjeCtlbWl1dTZDU2xHbG0xY0NPaUhhMzdONkpmUDVsOHY0V3VuNlRKNmxBK3FPa3hlaFJvaUNtdUdXdFJMOXZDMk5mR3NIaVJyMng0WjVOTjZtd0tPR1NoTWZ6M0c1aGZxQm9YWWZYeHhkSDdcLzRSeVFwVWRwaklrR3FmS0VEMUxHbWozajg3blJuOWVYV1loK2pcLzBIZG0rVnVGRkFNQUFBPT0iLCJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfTk9fUk9MRVMiXSwiZXhwIjoxNTQ5NTU3MDE1LCJpYXQiOjE1NDk1MjEwMTV9.pr03tzY68Uz4nst_Gl5fntES0Zg5tIan6M8aixUCIgc";
    }

    private eventSubject = new BehaviorSubject<any>(undefined);

    triggerSomeEvent(param: any) {
        this.eventSubject.next(param);
    }
   
    getEventSubject(): BehaviorSubject<any> {
       return this.eventSubject;
    }

getUmmUsers(): Observable<any> {

    this.spinner = true;
    this.sender.next(this.spinner);
    return this._httpClient.get(this.baseUrl1+"/umm/user/list" , {
        headers: new HttpHeaders({
            'Authorization': 'Bearer '+this.token
        })
    });
}

getUmmRoles(): Observable<any> {
    this.spinner = true;
    this.sender.next(this.spinner);
    return this._httpClient.get(this.baseUrl1+"/umm/role/list" , {
        headers: new HttpHeaders({
            'Authorization': 'Bearer '+this.token
        })
    });
}

addUmmAgent(agent): Observable<any>
{
    return this._httpClient.post<any>(this.baseUrl1+"/umm/user/create",agent, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json;charset=UTF-8',
             'Authorization': 'Bearer '+this.token
           })
         } )
}

UpdateProfilePic(image): Observable<any>
{
    return this._httpClient.post<any>(this.baseUrl1+"/umm/user/updateProfilePic",image, {
           headers: new HttpHeaders({
             'Authorization': 'Bearer '+this.token
           })
         } )
}

addRolesToUmmAgents(roles): Observable<any>
{
    return this._httpClient.put<any>(this.baseUrl1+"/umm/user/assignRoles",roles, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json',
             'Authorization': 'Bearer '+this.token
           })
        } )
}

deleteUmmAgent(id): Observable<any> {
    return this._httpClient.delete<any>(this.baseUrl1+"/umm/user/delete?id="+id, {
        headers: new HttpHeaders({
            'Content-Type' : 'application/json',
            'Authorization': 'Bearer '+this.token
        })
    })
}

updateUmmAgent(agent): Observable<any>
{
    return this._httpClient.put<any>(this.baseUrl1+"/umm/user/update",agent, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json',
             'Authorization': 'Bearer '+this.token
           })
        } )
}

revokeRoles(role): Observable<any>
{
    return this._httpClient.put<any>(this.baseUrl1+"/umm/user/revokeRoles",role, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json',
             'Authorization': 'Bearer '+this.token
           })
        } )
}

getMicroservices(): Observable<any> {

    return this._httpClient.get(this.baseUrl1+"/umm/microservice/list" , {
        headers: new HttpHeaders({
            'Authorization': 'Bearer '+this.token
        })
    });
}

CreateNewRole(role): Observable<any>
{
    return this._httpClient.post<any>(this.baseUrl1+"/umm/role/create",role, {
           headers: new HttpHeaders({
             'Authorization': 'Bearer '+this.token
           })
         } )
}

updateRole(role): Observable<any>
{
    return this._httpClient.put<any>(this.baseUrl1+"/umm/role/update",role, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json',
             'Authorization': 'Bearer '+this.token
           })
        } )
}

deleteRole(id): Observable<any> {
    return this._httpClient.delete<any>(this.baseUrl1+"/umm/role/delete?id="+id, {
        headers: new HttpHeaders({
            'Content-Type' : 'application/json',
            'Authorization': 'Bearer '+this.token
        })
    })
}

addRevokePermission(permission): Observable<any>
{
    return this._httpClient.put<any>(this.baseUrl1+"/umm/role/addRevokePermissions",permission, {
           headers: new HttpHeaders({
             'Content-Type' : 'application/json',
             'Authorization': 'Bearer '+this.token
           })
        } )
}


}