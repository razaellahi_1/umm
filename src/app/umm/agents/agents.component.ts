import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { service } from '../services';
import { NewAgentDialogComponent } from './new-agent-dialog/new-agent-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { Observable } from 'rxjs';
import { RolesComponent } from '../roles/roles.component';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.scss'],
  animations: [
    trigger('selectedImage', [
      state('hideImage', style({
        opacity: 0,
      })),
      state('showImage', style({
        opacity: 1
      })),
      transition('showImage=>hideImage',[
        animate(0)
      ]),
      transition('hideImage=>showImage',[
        animate(650)
      ])
    ])
  ]
})
export class AgentsComponent implements OnInit {

  
  panelOpenState = false;
  name = new FormControl('');
  email = new FormControl('');
  fullName = new FormControl('');
  imageState="showImage";
  ummRoles;
  selectedFile: File;
  image;
  agents;
  firstdate;
  customCollapsedHeight: string = '65px';
  customExpandedHeight: string = '65px';
  public spinner: boolean = false;
  connectionErrorImg="assets/images/error.jpg";
  searchItem;
  trackByAgentId = (index, agent) => agent.id;


  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: any = [];
  newArray: any = [];
  allFruits;
  @ViewChild('fruitInput') fruitInput: ElementRef;

  constructor(private dialog: MatDialog, private  _callService : service) { }
  ngOnInit() {

    this._callService.getEventSubject().subscribe((param: any) => {
      if (param !== undefined) {
        this.theTargetMethod(param);
      }
      });
    
   this.firstdate = Date.now();
   this.getUmmUsers();
   this.getUmmRoles(); 
  }
  onAddNewagentClick() {
    const dialogRef = this.dialog.open(NewAgentDialogComponent, {
      maxWidth: '724px',
      maxHeight: '341px',
     width: '724px',
     height: '341px'
   

    });

    dialogRef.afterClosed().subscribe(result => {
     
    });

  }


  getUmmUsers(){
    this._callService.cast.subscribe(e => this.spinner = e);
    this._callService.getUmmUsers().subscribe((e) => {
       this.agents = e.users;
    },
      (error) => {
        alert(error.message);
        this.spinner = false;
      },
      () => {
      this.spinner = false;
      });
  }

  onDeleteClick(AgentId){
    this._callService.deleteUmmAgent(AgentId).subscribe(
        () => {console.log("agent Deleted");
        this.getUmmUsers();
      },
        (error) => {console.log(error.message)}
    )
  }

  oldfileChangeEvent(input: any, name) {
    this.imageState="hideImage";
    
    this.selectedFile = input.target.files[0];  
    if (input.target.files && input.target.files.length>0) {
      var reader = new FileReader();
      reader.onload = (e:any) =>{
        this.image = e.target.result;
        this.imageState="showImage";
      };
      reader.readAsDataURL(input.target.files[0]);
    }
    let imgForm = new FormData();
    imgForm.append('file', this.selectedFile);
    imgForm.append('agentId', name);
                  this._callService.UpdateProfilePic(imgForm).subscribe(
                    (e) => {console.log("image updated");
                    let agentRolesObj= <any>[];
                    agentRolesObj.push(e.user);
                    this.agents = this.agents.map(h => agentRolesObj.find(o => o.id == h.id) || h);},
                    (error) => {console.log(error.message)}
                  )
  }

  OnUpdateClick(agent){
    let NewAgent;
    NewAgent={'id':agent.id,
             'fullName': this.fullName.value,
             'email':this.email.value,
             'isActive':agent.isActive,
             'profileExists':agent.profileExists,
             'updatedby': {"id":1}
            }

            console.log(JSON.stringify(NewAgent));
  
this._callService.updateUmmAgent(NewAgent).subscribe(
  (e) => {console.log("updated")},
(error) => {alert(error.message)})
  }

  add(event: MatChipInputEvent): void {
    debugger
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({
        id:Math.random(),
        name:value.trim()
      });
    }
  
    // Reset the input value
    if (input) {
      input.value = '';
    }
  
    this.fruitCtrl.setValue(null);
  
  }
  
  remove(fruit, indx): void {
    this.fruits.splice(indx, 1);
    this.newArray= this.fruits.map(res => {return {id: res.id}});
    console.log(JSON.stringify(this.newArray));
  
  
  }
  
  selected(event: MatAutocompleteSelectedEvent,id): void {
    this.fruits.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
    this.newArray= this.fruits.map(res => {return {id: res.id}});
    console.log(JSON.stringify(this.newArray));
    let obj={'id': id, 'roles': this.newArray};
    this._callService.addRolesToUmmAgents(obj).subscribe(
     (e) => {console.log("roles addede");
     let agentRolesObj= <any>[];
     agentRolesObj.push(e.user);
     this.agents = this.agents.map(h => agentRolesObj.find(o => o.id == h.id) || h);},
     (error) => {alert(error.message)}
    );
  }
  
  private _filter(value: any): any[] {
    return this.allFruits.filter(fruit => fruit.name.toLowerCase().includes(value));
  }

  getUmmRoles(){
  this._callService.getUmmRoles().subscribe(
    (e) => {this.ummRoles = e.roles;
            this. allFruits = this.ummRoles;
  
            this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
              startWith(null),
              map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }
  )
}

onRolesClick(role,id){
  let obj={'id': id, 'roles':[{'id' : role.id}]};
  this._callService.revokeRoles(obj).subscribe(
    (e) => {console.log("role deleted");
    let agentRolesObj= <any>[];
    agentRolesObj.push(e.user);
    this.agents = this.agents.map(h => agentRolesObj.find(o => o.id == h.id) || h);
            },
    (error) => {alert(error.message)}
  )
}

theTargetMethod(a){
  console.log("i am called")
  this.getUmmUsers();
}
  
}
