import { PipeTransform, Pipe } from "@angular/core";
@Pipe({
    name: 'agentFilterPipe',
    pure: true

})
export class AgentFilterClass implements PipeTransform {

    transform(agents: any, name: string): any[] {
        if (!agents || !name) {
            return agents;
        }
        return agents.filter(e => e.fullName.toLowerCase().indexOf(name.toLowerCase()) !== -1);
    }
}