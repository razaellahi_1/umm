import { PipeTransform, Pipe } from "@angular/core";
@Pipe({
    name: 'myPipe',
    pure: true

})
export class durationPipe implements PipeTransform{
    
    transform(firstDate:any , secondDate:any):any{
       if(!firstDate || !secondDate){
           return firstDate;
       }

       const dropdt:any = new Date(firstDate);
      const pickdt:any = new Date(secondDate);
          const time = +((firstDate - secondDate) / (24 * 3600 * 1000));
        //   console.log("this is currentTime "+firstDate);
        //   console.log("this is userTime "+secondDate);
         const a = time.toString().split('.',1);
        // console.log("time is "+a);
          return a;
    }
}