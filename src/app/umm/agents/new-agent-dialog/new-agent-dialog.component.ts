import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormControl, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { service } from '../../services';
import { map, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { Observable } from 'rxjs';
import { AgentsComponent } from '../agents.component';

export class ummUserInterface {
  fullName: any;
  email?: any;
  password: any;
  username: any;
  isActive?: any;
  profileExists?: any;
  createdBy: any;
}

export class agentRoles {
  id: any;
  roles: any
}

@Component({
  selector: 'app-new-agent-dialog',
  templateUrl: './new-agent-dialog.component.html',
  styleUrls: ['./new-agent-dialog.component.scss'],
  animations: [
    trigger('selectedImage', [
      state('hideImage', style({
        opacity: 0,
      })),
      state('showImage', style({
        opacity: 1
      })),
      transition('showImage=>hideImage', [
        animate(0)
      ]),
      transition('hideImage=>showImage', [
        animate(650)
      ])
    ])
  ]
})
export class NewAgentDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewAgentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private _callService: service) {


  }

  ummRoles;
  imgUrl = "assets/images/dummy.jpg";
  _searchItem: string;
  searchItemsArray: any;
  ummObj: ummUserInterface = <any>{};
  selectFile: File = null;
  dummyImage = 'assets/images/dummy.jpg';
  imageState = "showImage";



  username = new FormControl('', [Validators.required,
  Validators.minLength(3), Validators.maxLength(30)]);

  password = new FormControl('', [Validators.required,
  Validators.minLength(8), Validators.maxLength(30)]);

  fullname = new FormControl('', [Validators.required,
  Validators.minLength(3), Validators.maxLength(100)]);

  emaill = new FormControl('');


  ngOnInit() {

    this._callService.getUmmRoles().subscribe(
      (e) => {
      this.ummRoles = e.roles;
        this.allFruits = this.ummRoles;

        this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
          startWith(null),
          map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
      }
    )
  }
  triggerAnEvent() {
    this._callService.triggerSomeEvent('nm');
  }
  checkemail() {
    if (this.emaill.value && this.emaill.value != null) {
      this.emaill.setValidators(Validators.email);
      this.emaill.updateValueAndValidity();
    }
    else {
      this.emaill.clearValidators();
      this.emaill.updateValueAndValidity();
    }

  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: any = [];
  newArray: any = [];
  allFruits;
  @ViewChild('fruitInput') fruitInput: ElementRef;

  get searchItems() {
    return this._searchItem;
  }

  set searchItems(value: string) {

    this._searchItem = value;

    if (this.ummRoles && value) {
      this.searchItemsArray = this.ummRoles.filter(e => e.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }
  }

  makeuser() {
    this.ummObj.username = this.username.value;
    this.ummObj.fullName = this.fullname.value;
    this.ummObj.email = this.emaill.value;
    this.ummObj.password = this.password.value;
    this.ummObj.profileExists = true;
    this.ummObj.createdBy = { "id": 1 };

    this._callService.addUmmAgent(this.ummObj).subscribe(
      (ee) => {
        console.log("addd");
        this.triggerAnEvent();
        if (this.selectFile != null) {
          let body = new FormData();
          body.append('file', this.selectFile);
          body.append('agentId', ee.user.username);

          this._callService.UpdateProfilePic(body).subscribe(
            () => {
              console.log("image added");
              this.selectFile = null;
              this.triggerAnEvent();

            },
            (error) => {
              console.log(error.message);

            }
          )
        }
        if (this.newArray.length != 0) {
          let obj = { 'id': ee.user.id, 'roles': this.newArray };
          this._callService.addRolesToUmmAgents(obj).subscribe(
            () => {
              console.log("roles added");
              this.newArray = [];
              this.triggerAnEvent();
            },
            (error) => {
              console.log(error.message);
              this.newArray = [];
            }
          )
        }


      },
      (error) => { console.log(error.message) }
    )
  }

  fileChangeEventt(inputis: any) {
    this.imageState = "hideImage";

    this.selectFile = inputis.target.files[0];

    if (inputis.target.files && inputis.target.files.length > 0) {
      let reader = new FileReader();
      reader.onload = (e: any) => {
        this.dummyImage = e.target.result;
        this.imageState = "showImage";
      };
      reader.readAsDataURL(inputis.target.files[0]);
    }
  }

  add(event: MatChipInputEvent): void {
    debugger
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({
        id: Math.random(),
        name: value.trim()
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);

  }

  remove(fruit, indx): void {
    this.fruits.splice(indx, 1);
    this.newArray = this.fruits.map(res => { return { id: res.id } });


  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.value);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
    this.newArray = this.fruits.map(res => { return { id: res.id } });

  }

  private _filter(value: any): any[] {
    return this.allFruits.filter(fruit => fruit.name.toLowerCase().includes(value));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
