import { Component } from '@angular/core';
import {Router} from "@angular/router";
import 'hammerjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

constructor(private router: Router){}

  mode = 'push';
  sidenavWidth = 4;
  optionsHeader = false;

  increase() {
    this.sidenavWidth = 8;
  }

  decrease() {
    this.sidenavWidth = 4;
  }
}
