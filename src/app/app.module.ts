import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { AgentsComponent } from './umm/agents/agents.component';
import { RolesComponent } from './umm/roles/roles.component';
import { service } from './umm/services';
import { durationPipe } from './umm/agents/duration.pipe';
import { NewAgentDialogComponent } from './umm/agents/new-agent-dialog/new-agent-dialog.component';
import { AgentFilterClass } from './umm/agents/agents-filter.pipe';
import { CreateNewRoleComponent } from './umm/roles/create-new-role/create-new-role.component';
import { RolesFilterClass } from './umm/roles/roles-filter.pipe';


export const appRoutes: Routes = [

  { path: 'agents', component: AgentsComponent },
  { path: 'roles', component: RolesComponent },
  { path: '', redirectTo: '/agents', pathMatch: 'full' }

];

@NgModule({
  declarations: [
    AppComponent,
    AgentsComponent,
    RolesComponent,
    durationPipe,
    NewAgentDialogComponent,
    AgentFilterClass,
    CreateNewRoleComponent,
    RolesFilterClass
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    RouterModule.forRoot(appRoutes),
    MatDialogModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatChipsModule,
    MatFormFieldModule,
    HttpClientModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    AngularFontAwesomeModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  providers: [service],
  bootstrap: [AppComponent],
  entryComponents: [
    NewAgentDialogComponent,
    CreateNewRoleComponent
  ]
})
export class AppModule { }
